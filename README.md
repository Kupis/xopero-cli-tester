# Xopero CLI Tester
.NET Core application to test Xopero CLI.

## Requirements

* .NET Core 2.2,
* Xopero account (server address, login, password),
* opero-commandline.jar (known as Xopero CLI) in the same folder with this programm 

## Assumptions

Every 15 (or changed by user) minutes program will create random file (beetween 512 KB and 1 MB) and try to backup it with Xopero CLI. After that programm will rename created file and try to restore backuped file. Then will use SHA256 to verify checksum and print result. After test programm will remove server and local files.

Program support using arguments:
* /? ? help /h /help -h -help
* --server={https://ip:port}
* --server={ip:port}
* --login={login}
* --password={password}
* --timeout={seconds(int), default 30}
* --timer={minutes(int}, default 15}

## Author

* **Patryk Kupis** - [Linkedin](https://www.linkedin.com/in/patryk-kupis-12a453162/), [Gitlab](https://gitlab.com/Kupis)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Timers;
using System.Linq;

namespace Xopero_CLI_Tester
{
    class Program
    {
        private static string login;
        private static string password;
        private static string server;

        private static Timer timer;
        private static int minutes = 15;
        private static int defaultMinutes = 15;
        private static int timerSeconds = 30;
        private static int defaultTimerSeconds = 30;

        private static Guid filename;
        private static Guid copyFileName;
        private static string directory;
        private static int fileSize;

        private static bool testSuccess = false;

        private static ConsoleColor defaultColor;

        private static Random random = new Random();

        static void Main(string[] args)
        {
            try
            {
                bool argSever = false;
                bool argLogin = false;
                bool argPassword = false;
                bool argTimeOut = false;
                bool argTimer = false;

                if (args.Length > 0)
                {
                    if (args[0].StartsWith("/?") || args[0].StartsWith("?") || args[0].StartsWith("help") || args[0].StartsWith("/h") || args[0].StartsWith("-h"))
                    {
                        Console.WriteLine("Programm support arguments:");
                        Console.WriteLine("--server={https://ip:port} --login={login} --password={password} --timeout={minutes(int)} --timer={seconds(int)}" + Environment.NewLine);
                        Console.ReadLine();
                        Environment.Exit(-1);
                    }
                }

                directory = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\";

                if (args.Length > 0)
                {
                    foreach (string arg in args)
                    {
                        if (arg.StartsWith("--server="))
                        {
                            server = arg.Substring(9);
                            argSever = true;
                        }
                        else if (arg.StartsWith("--login="))
                        {
                            login = arg.Substring(8);
                            argLogin = true;
                        }
                        else if (arg.StartsWith("--password="))
                        {
                            password = arg.Substring(11);
                            argPassword = true;
                        }
                        else if (arg.StartsWith("--timeout="))
                        {
                            string timeout = arg.Substring(10);
                            if (int.TryParse(timeout, out minutes))
                                argTimeOut = true;
                        }
                        else if (arg.StartsWith("--timer="))
                        {
                            string timer = arg.Substring(8);
                            if (int.TryParse(timer, out timerSeconds))
                                argTimer = true;
                        }
                    }
                }

                if (!argSever)
                {
                    Console.WriteLine("Please enter server address (https://ip:port)");
                    server = Console.ReadLine();
                }

                if (!argLogin)
                {
                    Console.WriteLine("Please enter login");
                    login = Console.ReadLine();
                }

                if (!argPassword)
                {
                    Console.WriteLine("Please enter password");
                    password = Console.ReadLine();
                }
                
                if (!argTimeOut)
                {
                    Console.WriteLine("Please enter seconds to timeout or leave empty for default 30");
                    string timeout = Console.ReadLine();
                    if (!String.IsNullOrWhiteSpace(timeout))
                    {
                        if (int.TryParse(timeout, out minutes))
                        { }
                        else
                        {
                            minutes = defaultMinutes;
                            Console.WriteLine("Parse from string to int failed, leaving default value");
                        }
                    }
                }

                if (!argTimer)
                {
                    Console.WriteLine("Please enter minutes for timer or leave empty for default 15");
                    string timer = Console.ReadLine();
                    if (!String.IsNullOrWhiteSpace(timer))
                    {
                        if (int.TryParse(timer, out timerSeconds))
                        { }
                        else
                        {
                            timerSeconds = defaultTimerSeconds;
                            Console.WriteLine("Parse from string to int failed, leaving default value");
                        }
                    }
                }
                defaultColor = Console.ForegroundColor;

                Console.Clear();

                if (!CheckConnection())
                {
                    ErrorMessage("Can not connect to server! Press any key to close program");
                    Console.ReadLine();

                    Environment.Exit(-1);
                }
                else
                    SuccessMessage("Connected");

                CreateHostName();

                SetTimer();

                Console.WriteLine("The application started at {0:HH:mm:ss.fff}", DateTime.Now);
                Console.WriteLine($"\nEvery {minutes.ToString()} min program will test a Xopero CLI. To exit the application press Enter key.\n");
                CreateRandomFile();

                Console.ReadLine();
                timer.Stop();
                timer.Dispose();

                Console.WriteLine("Terminating the application...");
                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex.Message);
            }
        }

        private static void SetTimer()
        {
            timer = new Timer(TimeSpan.FromMinutes(minutes).TotalMilliseconds);
            timer.Elapsed += Timer_Elapsed;
            timer.AutoReset = true;
            timer.Start();
        }

        private static bool CheckConnection()
        {
            try
            {
                string query = $"-jar opero-commandline.jar getaccountinfo --login={login} --password={password} --server={server}";
                var processInfo = new ProcessStartInfo("java.exe", query)
                {
                    RedirectStandardError = true,
                    RedirectStandardOutput = true
                };

                using (Process proc = Process.Start(processInfo))
                {
                    proc.WaitForExit((int)TimeSpan.FromSeconds(timerSeconds).TotalMilliseconds);

                    Console.WriteLine(proc.StandardError.ReadToEnd());

                    if (proc.ExitCode == 0)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex.Message);
                return false;
            }
        }

        private static void CreateHostName()
        {
            try
            {
                Console.WriteLine("Connecting to Xopero server and trying to create hostname.");
                string query = $"-jar opero-commandline.jar addhost --login={login} --password={password} --server={server} --host-name=CLI-Tester";
                var processInfo = new ProcessStartInfo("java.exe", query)
                {
                    RedirectStandardError = true,
                    RedirectStandardOutput = true
                };

                using (Process proc = Process.Start(processInfo))
                {
                    proc.WaitForExit((int)TimeSpan.FromSeconds(timerSeconds).TotalMilliseconds);

                    if (proc.ExitCode == 0)
                        Console.WriteLine("Created new hostname");
                    else
                        Console.WriteLine("Hostname exsists");
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex.Message);
            }
        }

        private static void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            CreateRandomFile();
        }

        private static void CreateRandomFile()
        {
            try
            {
                Console.WriteLine("### TEST STARTED AT {0} ###", DateTime.Now);

                filename = Guid.NewGuid();
                fileSize = random.Next(524288, 1048576);

                Byte[] bytes = new Byte[fileSize];
                random.NextBytes(bytes);

                File.WriteAllBytes(filename.ToString(), bytes);

                Console.WriteLine("- created file: {0}", filename);
                CreateBackupWithXoperoCLI();
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex.Message);
            }
        }

        private static void CreateBackupWithXoperoCLI()
        {
            try
            {
                string query = $"-jar opero-commandline.jar backup --login={login} --password={password} --server={server} --host-name=CLI-Tester --project-name=Test --path=\"{directory + filename}\"";
                var processInfo = new ProcessStartInfo("java.exe", query)
                {
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true
                };

                using (Process proc = Process.Start(processInfo))
                {
                    proc.WaitForExit((int)TimeSpan.FromSeconds(timerSeconds).TotalMilliseconds);

                    string lastLine = "";
                    string output = "";

                    while (!proc.StandardOutput.EndOfStream)
                    {
                        lastLine = proc.StandardOutput.ReadLine();
                        output += lastLine;
                    }

                    if (proc.ExitCode == 0)
                    {
                        Console.WriteLine("- created backup using XoperoCli");
                        RestoreBackupWithXoperoCLI();
                    }
                    else
                    {
                        ErrorMessage(proc.StandardError.ReadToEnd());
                        ErrorMessage($"Error during creating backup with XoperoCLI");
                        EndTest();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex.Message);
            }
        }

        private static void RestoreBackupWithXoperoCLI()
        {
            try
            {
                copyFileName = Guid.NewGuid();
                File.Move(filename.ToString(), copyFileName.ToString());

                Console.WriteLine("- moved original file to {0}", copyFileName);

                string query = $"-jar opero-commandline.jar restore --login={login} --password={password} --server={server} --host-name=CLI-Tester --project-name=Test --path=\"{directory + filename}\"";
                var processInfo = new ProcessStartInfo("java.exe", query)
                {
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true
                };

                using (Process proc = Process.Start(processInfo))
                {
                    proc.WaitForExit((int)TimeSpan.FromSeconds(timerSeconds).TotalMilliseconds);

                    if (proc.ExitCode == 0)
                    {
                        Console.WriteLine("- restored backup using XoperoCLI");
                        CheckSum();
                    }
                    else
                    {
                        ErrorMessage(proc.StandardError.ReadToEnd());
                        ErrorMessage("Error during restoring backup with XoperoCLI");
                        EndTest();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex.Message);
            }
        }

        private static void CheckSum()
        {
            string firstChecksum;
            string secondChecksum;

            try
            {
                using (SHA256 sha256 = SHA256Managed.Create())
                {
                    using (var stream = File.OpenRead(filename.ToString()))
                    {
                        var hash = sha256.ComputeHash(stream);
                        firstChecksum = BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                    }
                }

                using (SHA256 sha256 = SHA256Managed.Create())
                {
                    using (var stream = File.OpenRead(copyFileName.ToString()))
                    {
                        var hash = sha256.ComputeHash(stream);
                        secondChecksum = BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                    }
                }

                if (firstChecksum == secondChecksum)
                {
                    testSuccess = true;
                    SuccessMessage("- checksum is correct");
                }
                else
                {
                    ErrorMessage("- checksum is inncorect!");
                    ErrorMessage($"- first file checksum: {firstChecksum}");
                    ErrorMessage($"- second file checksum: {secondChecksum}");
                    EndTest();
                }

                Console.WriteLine("- starting cleaning memory");
                ClearMemory();
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex.Message);
            }
        }

        private static void ClearMemory()
        {
            try
            {
                string query = $"-jar opero-commandline.jar delete --login={login} --password={password} --server={server} --host-name=CLI-Tester --project-name=Test --path=\"{directory + filename}\"";
                var processInfo = new ProcessStartInfo("java.exe", query)
                {
                    RedirectStandardInput = true,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true
                };

                using (Process proc = Process.Start(processInfo))
                {
                    StreamWriter sw = proc.StandardInput;
                    sw.WriteLine("yes");
                    proc.WaitForExit((int)TimeSpan.FromSeconds(timerSeconds).TotalMilliseconds);

                    if (proc.ExitCode == 0)
                    {
                        Console.WriteLine("- deleted backup from Xopero server");
                    }
                    else
                    {
                        ErrorMessage(proc.StandardError.ReadToEnd());
                        ErrorMessage("Error during clearing memory from Xopero server");
                        EndTest();
                    }
                }

                File.Delete(filename.ToString());
                File.Delete(copyFileName.ToString());

                Console.WriteLine("- deleted local files");

                EndTest();
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex.Message);
            }
        }

        private static void EndTest()
        {
            if (testSuccess)
                SuccessMessage("TEST RESULT: SUCCESS");
            else
                ErrorMessage("TEST RESULT: FAIL");

            testSuccess = false;

            Console.WriteLine("###END OF TEST {0} ###" + Environment.NewLine, DateTime.Now);
        }

        private static void ErrorMessage(string msg)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(msg);
            Console.ForegroundColor = defaultColor;
        }

        private static void SuccessMessage(string msg)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(msg);
            Console.ForegroundColor = defaultColor;
        }

        private static void ExceptionMessage(string msg)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(msg);
            Console.ForegroundColor = defaultColor;
        }
    }
}
